package com.zy.community.community.service.impl;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.community.domain.ZyRepair;
import com.zy.community.community.domain.dto.ZyRepairDto;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.mapper.ZyRepairMapper;
import com.zy.community.community.service.IZyRepairService;
import com.zy.community.framework.security.mini.MiniContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 报修信息Service业务层处理
 * 
 * @author yin
 * @date 2020-12-22
 */
@Service
public class ZyRepairServiceImpl implements IZyRepairService
{
    @Autowired
    private ZyRepairMapper zyRepairMapper;

    @Resource
    private ZyOwnerMapper zyOwnerMapper;

    @Value("${community.value}")
    private String propertyId;

    /**
     * 查询报修信息
     * 
     * @param repairId 报修信息ID
     * @return 报修信息
     */
    @Override
    public ZyRepair selectZyRepairById(Long repairId)
    {
        return zyRepairMapper.selectById(repairId);
    }

    /**
     * 查询报修信息列表
     * 
     * @param zyRepair 报修信息
     * @return 报修信息
     */
    @Override
    public List<ZyRepairDto> selectZyRepairList(ZyRepairDto zyRepair)
    {
        zyRepair.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        return zyRepairMapper.selectZyRepairById(zyRepair);
    }

    /**
     * 新增报修信息
     * 
     * @param zyRepair 报修信息
     * @return 结果
     */
    @Override
    public int insertZyRepair(ZyRepair zyRepair)
    {
        zyRepair.setCreateTime(DateUtils.getNowDate());
        Long ownerId = zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId());
        zyRepair.setUserId(ownerId);
        zyRepair.setRepairNum("BX_"+System.currentTimeMillis());
        return zyRepairMapper.insert(zyRepair);
    }

    /**
     * 修改报修信息
     * 
     * @param zyRepair 报修信息
     * @return 结果
     */
    @Override
    public int updateZyRepair(ZyRepair zyRepair)
    {
        zyRepair.setUpdateTime(DateUtils.getNowDate());
        Long ownerId = zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId());
        zyRepair.setUpdateBy(ownerId + "");
        return zyRepairMapper.updateById(zyRepair);
    }

    /**
     * 批量删除报修信息
     * 
     * @param repairIds 需要删除的报修信息ID
     * @return 结果
     */
    @Override
    public int deleteZyRepairByIds(Long[] repairIds)
    {
        return zyRepairMapper.deleteById(repairIds);
    }

    /**
     * 删除报修信息信息
     * 
     * @param repairId 报修信息ID
     * @return 结果
     */
    @Override
    public int deleteZyRepairById(Long repairId)
    {
        return zyRepairMapper.deleteById(repairId);
    }
}

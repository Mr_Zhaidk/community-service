package com.zy.community.community.service;

import com.zy.community.community.domain.ZyRepair;
import com.zy.community.community.domain.dto.ZyRepairDto;

import java.util.List;

/**
 * 报修信息Service接口
 * 
 * @author yin
 * @date 2020-12-22
 */
public interface IZyRepairService 
{
    /**
     * 查询报修信息
     * 
     * @param repairId 报修信息ID
     * @return 报修信息
     */
    public ZyRepair selectZyRepairById(Long repairId);

    /**
     * 查询报修信息列表
     * 
     * @param zyRepair 报修信息
     * @return 报修信息集合
     */
    public List<ZyRepairDto> selectZyRepairList(ZyRepairDto zyRepair);

    /**
     * 新增报修信息
     * 
     * @param zyRepair 报修信息
     * @return 结果
     */
    public int insertZyRepair(ZyRepair zyRepair);

    /**
     * 修改报修信息
     * 
     * @param zyRepair 报修信息
     * @return 结果
     */
    public int updateZyRepair(ZyRepair zyRepair);

    /**
     * 批量删除报修信息
     * 
     * @param repairIds 需要删除的报修信息ID
     * @return 结果
     */
    public int deleteZyRepairByIds(Long[] repairIds);

    /**
     * 删除报修信息信息
     * 
     * @param repairId 报修信息ID
     * @return 结果
     */
    public int deleteZyRepairById(Long repairId);
}

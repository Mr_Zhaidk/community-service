package com.zy.community.community.service.impl;

import com.zy.community.common.annotation.DataScope;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.utils.SecurityUtils;
import com.zy.community.community.domain.ZyCommunity;
import com.zy.community.community.domain.dto.ZyCommunityDto;
import com.zy.community.community.mapper.ZyCommunityMapper;
import com.zy.community.community.service.IZyCommunityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author yangdi
 */
@Service
public class IZyCommunityServiceImpl implements IZyCommunityService {

    @Resource
    private ZyCommunityMapper communityMapper;

    private static final String CODE_PREFIX = "COMMUNITY_";

    @Override
    @DataScope(deptAlias = "d")
    public List<ZyCommunityDto> selectZyCommunityList(ZyCommunity zyCommunity) {
        return communityMapper.queryList(zyCommunity);
    }

    @Override
    public ZyCommunity selectZyCommunityById(Long communityId) {
        return communityMapper.selectById(communityId);
    }

    @Override
    public int insertZyCommunity(ZyCommunity zyCommunity){
        zyCommunity.setCommunityCode(CODE_PREFIX+System.currentTimeMillis());
        zyCommunity.setDeptId(SecurityUtils.getLoginUser().getUser().getDeptId());
        return communityMapper.insert(ManageParametersUtils.saveMethod(zyCommunity));
    }

    @Override
    public int updateZyCommunity(ZyCommunity zyCommunity) {
        return communityMapper.updateById(ManageParametersUtils.updateMethod(zyCommunity));
    }

    @Override
    public int deleteZyCommunityByIds(Long[] communityIds) {
        return communityMapper.deleteBatchIds(Arrays.asList(communityIds));
    }
}

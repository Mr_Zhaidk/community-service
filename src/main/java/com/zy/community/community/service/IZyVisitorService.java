package com.zy.community.community.service;

import com.zy.community.community.domain.ZyVisitor;
import com.zy.community.community.domain.dto.ZyVisitorDto;

import java.util.List;

/**
 * 访客邀请 Service接口
 *
 * @author yangdi
 * @date 2020-12-18
 */
public interface IZyVisitorService {
    /**
     * 查询访客邀请 列表
     *
     * @param zyVisitor 访客邀请
     * @return 访客邀请 集合
     */
    public List<ZyVisitorDto> selectZyVisitorList(ZyVisitor zyVisitor);

}

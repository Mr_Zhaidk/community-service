package com.zy.community.community.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.community.domain.ZyComment;
import com.zy.community.community.domain.dto.ZyCommentDto;
import com.zy.community.community.mapper.ZyCommentMapper;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.service.IZyCommentService;
import com.zy.community.framework.security.mini.MiniContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 评论Service业务层处理
 * 
 * @author yin
 * @date 2020-12-18
 */
@Service
public class ZyCommentServiceImpl implements IZyCommentService
{
    @Autowired
    private ZyCommentMapper zyCommentMapper;
    @Autowired
    private ZyOwnerMapper zyOwnerMapper;

    /**
     * 查询评论
     * 
     * @param commentId 评论ID
     * @return 评论
     */
    @Override
    public ZyComment selectZyCommentById(Long commentId)
    {
        return zyCommentMapper.selectById(commentId);
    }

    /**
     * 查询评论列表
     * 
     * @param zyComment 评论
     * @return 评论
     */
    @Override
    public List<ZyCommentDto> selectZyCommentList(ZyCommentDto zyComment)
    {
        return zyCommentMapper.selectZyCommentByInteractionId(zyComment);
    }

    /**
     * 新增评论
     * 
     * @param zyComment 评论
     * @return 结果
     */
    @Override
    public int insertZyComment(ZyComment zyComment)
    {
        zyComment.setCreateTime(DateUtils.getNowDate());
        Long ownerId = zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId());
        zyComment.setUserId(ownerId);
        return zyCommentMapper.insert(zyComment);
    }

    /**
     * 修改评论
     * 
     * @param zyComment 评论
     * @return 结果
     */
    @Override
    public int updateZyComment(ZyComment zyComment)
    {
        zyComment.setUpdateTime(DateUtils.getNowDate());
        return zyCommentMapper.updateById(zyComment);
    }

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的评论ID
     * @return 结果
     */
    @Override
    public int deleteZyCommentByIds(Long[] commentIds)
    {
        return zyCommentMapper.deleteById(commentIds);
    }

    /**
     * 删除评论信息
     * 
     * @param commentId 评论ID
     * @return 结果
     */
    @Override
    public int deleteZyCommentById(Long commentId)
    {
        ZyComment zyComment = new ZyComment();
        zyComment.setDelFlag(1);//删除状态
        zyComment.setCommentId(commentId);
        return zyCommentMapper.updateById(zyComment);
    }
}

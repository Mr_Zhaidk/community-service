package com.zy.community.community.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.community.domain.ZyVisitor;
import com.zy.community.community.domain.dto.ZyVisitorDto;
import com.zy.community.community.mapper.ZyVisitorMapper;
import com.zy.community.community.service.IZyVisitorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 访客邀请 Service业务层处理
 *
 * @author yangdi
 * @date 2020-12-18
 */
@Service
public class ZyVisitorServiceImpl implements IZyVisitorService {
    @Resource
    private ZyVisitorMapper zyVisitorMapper;

    @Value("${community.value}")
    private String propertyId;

    /**
     * 查询访客邀请 列表
     *
     * @param zyVisitor 访客邀请
     * @return 访客邀请
     */
    @Override
    public List<ZyVisitorDto> selectZyVisitorList(ZyVisitor zyVisitor) {
        zyVisitor.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        return zyVisitorMapper.selectZyVisitorList(zyVisitor);
    }
}

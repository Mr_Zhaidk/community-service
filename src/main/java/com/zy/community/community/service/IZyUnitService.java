package com.zy.community.community.service;

import com.zy.community.community.domain.ZyUnit;
import com.zy.community.community.domain.dto.ZyUnitDto;

import java.util.List;

/**
 * 单元 Service接口
 * 
 * @author ruoyi
 * @date 2020-12-11
 */
public interface IZyUnitService 
{
    /**
     * 查询单元 
     * 
     * @param unitId 单元 ID
     * @return 单元 
     */
    public ZyUnit selectZyUnitById(Long unitId);

    /**
     * 查询单元 列表
     * 
     * @param zyUnit 单元 
     * @return 单元 集合
     */
    public List<ZyUnitDto> selectZyUnitList(ZyUnit zyUnit);

    /**
     * 新增单元 
     * 
     * @param zyUnit 单元 
     * @return 结果
     */
    public int insertZyUnit(ZyUnit zyUnit);

    /**
     * 修改单元 
     * 
     * @param zyUnit 单元 
     * @return 结果
     */
    public int updateZyUnit(ZyUnit zyUnit);

    /**
     * 批量删除单元 
     * 
     * @param unitIds 需要删除的单元 ID
     * @return 结果
     */
    public int deleteZyUnitByIds(Long[] unitIds);

    /**
     * 删除单元 信息
     * 
     * @param unitId 单元 ID
     * @return 结果
     */
    public int deleteZyUnitById(Long unitId);
}

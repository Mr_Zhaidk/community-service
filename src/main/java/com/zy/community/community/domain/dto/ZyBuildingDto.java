package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.ZyUnit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdi
 */
public class ZyBuildingDto extends BaseEntity {

    /** 楼栋id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    /** 楼栋名称 */
    private String buildingName;

    /** 楼栋编码 */
    private String buildingCode;

    /** 楼栋面积 */
    private BigDecimal buildingAcreage;

    /** 小区id */
    private Long communityId;

    private List<ZyUnit> list = new ArrayList<>();

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public BigDecimal getBuildingAcreage() {
        return buildingAcreage;
    }

    public void setBuildingAcreage(BigDecimal buildingAcreage) {
        this.buildingAcreage = buildingAcreage;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public List<ZyUnit> getList() {
        return list;
    }

    public void setList(List<ZyUnit> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ZyBuildingDto{" +
                "buildingId=" + buildingId +
                ", buildingName='" + buildingName + '\'' +
                ", buildingCode='" + buildingCode + '\'' +
                ", buildingAcreage=" + buildingAcreage +
                ", communityId=" + communityId +
                ", list=" + list +
                '}';
    }
}

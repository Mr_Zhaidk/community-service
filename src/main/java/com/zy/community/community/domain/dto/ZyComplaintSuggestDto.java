package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.domain.vo.ComplaintSuggestType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 投诉建议 对象 zy_complaint_suggest
 *
 * @author yangdi
 * @date 2020-12-18
 */
public class ZyComplaintSuggestDto extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long complaintSuggestId;

    /**
     * 小区id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 类型(投诉、建议)
     */
    @Excel(name = "类型(投诉、建议)")
    private ComplaintSuggestType complaintSuggestType;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String complaintSuggestContent;

    /**图片地址list*/
    private List urlList;

    /**用户昵称*/
    private String ownerNickname;

    /**用户真实姓名*/
    private String ownerRealName;

    /**业主电话*/
    private String ownerPhoneNumber;

    public String getOwnerNickname() {
        return ownerNickname;
    }

    public void setOwnerNickname(String ownerNickname) {
        this.ownerNickname = ownerNickname;
    }

    public String getOwnerRealName() {
        return ownerRealName;
    }

    public void setOwnerRealName(String ownerRealName) {
        this.ownerRealName = ownerRealName;
    }

    public String getOwnerPhoneNumber() {
        return ownerPhoneNumber;
    }

    public void setOwnerPhoneNumber(String ownerPhoneNumber) {
        this.ownerPhoneNumber = ownerPhoneNumber;
    }

    public List getUrlList() {
        return urlList;
    }

    public void setUrlList(List urlList) {
        this.urlList = urlList;
    }

    public void setComplaintSuggestId(Long complaintSuggestId) {
        this.complaintSuggestId = complaintSuggestId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getComplaintSuggestId() {
        return complaintSuggestId;
    }

    public ComplaintSuggestType getComplaintSuggestType() {
        return complaintSuggestType;
    }

    public void setComplaintSuggestType(ComplaintSuggestType complaintSuggestType) {
        this.complaintSuggestType = complaintSuggestType;
    }

    public void setComplaintSuggestContent(String complaintSuggestContent) {
        this.complaintSuggestContent = complaintSuggestContent;
    }

    public String getComplaintSuggestContent() {
        return complaintSuggestContent;
    }

    @Override
    public String toString() {
        return "ZyComplaintSuggestDto{" +
                "complaintSuggestId=" + complaintSuggestId +
                ", communityId=" + communityId +
                ", complaintSuggestType=" + complaintSuggestType +
                ", complaintSuggestContent='" + complaintSuggestContent + '\'' +
                ", urlList=" + urlList +
                '}';
    }
}

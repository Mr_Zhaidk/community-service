package com.zy.community.community.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.RoomStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 房屋绑定 对象 zy_owner_room
 *
 * @author yangdi
 * @date 2020-12-15
 */
public class ZyOwnerRoom extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 房屋绑定id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId
    private Long ownerRoomId;

    /**
     * 小区id
     */
    @Excel(name = "小区id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 楼栋id
     */
    @Excel(name = "楼栋id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    /**
     * 单元id
     */
    @Excel(name = "单元id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    /**
     * 房间id
     */
    @Excel(name = "房间id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;

    /**
     * 业主id
     */
    @Excel(name = "业主id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    /**
     * 业主类型
     */
    @Excel(name = "业主类型")
    private String ownerType;

    /**
     * 绑定状态（0审核中 1绑定 2审核失败）
     */
    @Excel(name = "绑定状态", readConverterExp = "0=审核中,1=绑定,2=审核失败")
    private RoomStatus roomStatus;

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("ownerRoomId", getOwnerRoomId())
                .append("communityId", getCommunityId())
                .append("buildingId", getBuildingId())
                .append("unitId", getUnitId())
                .append("roomId", getRoomId())
                .append("ownerId", getOwnerId())
                .append("ownerType", getOwnerType())
                .append("roomStatus", getRoomStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}

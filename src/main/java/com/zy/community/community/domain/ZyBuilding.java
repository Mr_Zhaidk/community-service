package com.zy.community.community.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 楼栋 对象 zy_building
 *
 * @author yangdi
 * @date 2020-12-11
 */
public class ZyBuilding extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 楼栋id
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Excel(name = "楼栋id")
    private Long buildingId;

    /**
     * 楼栋名称
     */
    @Excel(name = "楼栋名称")
    private String buildingName;

    /**
     * 楼栋编码
     */
    @Excel(name = "楼栋编码")
    private String buildingCode;

    /**
     * 楼栋面积
     */
    @Excel(name = "楼栋面积")
    private BigDecimal buildingAcreage;

    /**
     * 小区id
     */
    @Excel(name = "小区id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingAcreage(BigDecimal buildingAcreage) {
        this.buildingAcreage = buildingAcreage;
    }

    public BigDecimal getBuildingAcreage() {
        return buildingAcreage;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("buildingId", getBuildingId())
                .append("buildingName", getBuildingName())
                .append("buildingCode", getBuildingCode())
                .append("buildingAcreage", getBuildingAcreage())
                .append("communityId", getCommunityId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}

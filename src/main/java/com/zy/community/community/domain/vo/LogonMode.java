package com.zy.community.community.domain.vo;

/**
 * 注册方式
 */
public enum  LogonMode {
    /**
     * 微信注册
     */
    WeChat,
    /**
     * App注册
     */
    App,
    /**
     * web端注册
     */
    Web;
}

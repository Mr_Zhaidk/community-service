package com.zy.community.community.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.Gender;
import com.zy.community.community.domain.vo.LogonMode;
import com.zy.community.community.domain.vo.OwnerStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 业主 对象 zy_owner
 * 
 * @author ruoyi
 * @date 2020-12-10
 */
public class ZyOwner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业主id */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    /** 昵称 */
    @Excel(name = "昵称")
    private String ownerNickname;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String ownerRealName;

    /** 性别0默认1男2女 */
    @Excel(name = "性别0默认1男2女")
    private Gender ownerGender;

    /** 年龄 */
    @Excel(name = "年龄")
    private Integer ownerAge;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String ownerIdCard;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String ownerPhoneNumber;

    /** openid */
    @Excel(name = "openid")
    private String ownerOpenId;

    /** 微信号 */
    @Excel(name = "微信号")
    private String ownerWechatId;

    /** qq号码 */
    @Excel(name = "qq号码")
    private String ownerQqNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ownerBirthday;

    /** 头像 */
    @Excel(name = "头像")
    private String ownerPortrait;

    /** 个性签名 */
    @Excel(name = "个性签名")
    private String ownerSignature;

    /** 禁用状态enable启用-disable禁用 */
    @Excel(name = "禁用状态enable启用-disable禁用")
    private OwnerStatus ownerStatus;

    /** 注册方式（1微信2app3web） */
    @Excel(name = "注册方式", readConverterExp = "weChat=微信-app-web")
    private LogonMode ownerLogonMode;

    /** 业主类型 */
    @Excel(name = "业主类型")
    private String ownerType;

    /** 密码 */
    @Excel(name = "密码")
    private String ownerPassword;

    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setOwnerNickname(String ownerNickname) 
    {
        this.ownerNickname = ownerNickname;
    }

    public String getOwnerNickname() 
    {
        return ownerNickname;
    }
    public void setOwnerRealName(String ownerRealName) 
    {
        this.ownerRealName = ownerRealName;
    }

    public String getOwnerRealName() 
    {
        return ownerRealName;
    }

    public Gender getOwnerGender() {
        return ownerGender;
    }

    public void setOwnerGender(Gender ownerGender) {
        this.ownerGender = ownerGender;
    }

    public void setOwnerAge(Integer ownerAge)
    {
        this.ownerAge = ownerAge;
    }

    public Integer getOwnerAge() 
    {
        return ownerAge;
    }
    public void setOwnerIdCard(String ownerIdCard) 
    {
        this.ownerIdCard = ownerIdCard;
    }

    public String getOwnerIdCard() 
    {
        return ownerIdCard;
    }
    public void setOwnerPhoneNumber(String ownerPhoneNumber) 
    {
        this.ownerPhoneNumber = ownerPhoneNumber;
    }

    public String getOwnerPhoneNumber() 
    {
        return ownerPhoneNumber;
    }
    public void setOwnerOpenId(String ownerOpenId) 
    {
        this.ownerOpenId = ownerOpenId;
    }

    public String getOwnerOpenId() 
    {
        return ownerOpenId;
    }
    public void setOwnerWechatId(String ownerWechatId) 
    {
        this.ownerWechatId = ownerWechatId;
    }

    public String getOwnerWechatId() 
    {
        return ownerWechatId;
    }
    public void setOwnerQqNumber(String ownerQqNumber) 
    {
        this.ownerQqNumber = ownerQqNumber;
    }

    public String getOwnerQqNumber() 
    {
        return ownerQqNumber;
    }
    public void setOwnerBirthday(Date ownerBirthday) 
    {
        this.ownerBirthday = ownerBirthday;
    }

    public Date getOwnerBirthday() 
    {
        return ownerBirthday;
    }
    public void setOwnerPortrait(String ownerPortrait) 
    {
        this.ownerPortrait = ownerPortrait;
    }

    public String getOwnerPortrait() 
    {
        return ownerPortrait;
    }
    public void setOwnerSignature(String ownerSignature) 
    {
        this.ownerSignature = ownerSignature;
    }

    public String getOwnerSignature() 
    {
        return ownerSignature;
    }

    public OwnerStatus getOwnerStatus() {
        return ownerStatus;
    }

    public void setOwnerStatus(OwnerStatus ownerStatus) {
        this.ownerStatus = ownerStatus;
    }

    public LogonMode getOwnerLogonMode() {
        return ownerLogonMode;
    }

    public void setOwnerLogonMode(LogonMode ownerLogonMode) {
        this.ownerLogonMode = ownerLogonMode;
    }

    public void setOwnerType(String ownerType)
    {
        this.ownerType = ownerType;
    }

    public String getOwnerType() 
    {
        return ownerType;
    }
    public void setOwnerPassword(String ownerPassword) 
    {
        this.ownerPassword = ownerPassword;
    }

    public String getOwnerPassword() 
    {
        return ownerPassword;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("ownerId", getOwnerId())
            .append("ownerNickname", getOwnerNickname())
            .append("ownerRealName", getOwnerRealName())
            .append("ownerGender", getOwnerGender())
            .append("ownerAge", getOwnerAge())
            .append("ownerIdCard", getOwnerIdCard())
            .append("ownerPhoneNumber", getOwnerPhoneNumber())
            .append("ownerOpenId", getOwnerOpenId())
            .append("ownerWechatId", getOwnerWechatId())
            .append("ownerQqNumber", getOwnerQqNumber())
            .append("ownerBirthday", getOwnerBirthday())
            .append("ownerPortrait", getOwnerPortrait())
            .append("ownerSignature", getOwnerSignature())
            .append("ownerStatus", getOwnerStatus())
            .append("ownerLogonMode", getOwnerLogonMode())
            .append("ownerType", getOwnerType())
            .append("ownerPassword", getOwnerPassword())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}

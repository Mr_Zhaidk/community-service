package com.zy.community.community.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 单元 对象 zy_unit
 * 
 * @author ruoyi
 * @date 2020-12-11
 */
public class ZyUnit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 单元id */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Excel(name = "单元id")
    private Long unitId;

    /** 小区id */
    @Excel(name = "小区id")
    private Long communityId;

    /** 楼栋id */
    @Excel(name = "楼栋id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    /** 单元名称 */
    @Excel(name = "单元名称")
    private String unitName;

    /** 单元编号 */
    @Excel(name = "单元编号")
    private String unitCode;

    /** 层数 */
    @Excel(name = "层数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private int unitLevel;

    /** 建筑面积 */
    @Excel(name = "建筑面积")
    private BigDecimal unitAcreage;

    /** 是否有电梯 */
    @Excel(name = "是否有电梯")
    private String unitHaveElevator;

    public void setUnitId(Long unitId) 
    {
        this.unitId = unitId;
    }

    public Long getUnitId() 
    {
        return unitId;
    }
    public void setCommunityId(Long communityId) 
    {
        this.communityId = communityId;
    }

    public Long getCommunityId() 
    {
        return communityId;
    }
    public void setBuildingId(Long buildingId) 
    {
        this.buildingId = buildingId;
    }

    public Long getBuildingId() 
    {
        return buildingId;
    }
    public void setUnitName(String unitName) 
    {
        this.unitName = unitName;
    }

    public String getUnitName() 
    {
        return unitName;
    }
    public void setUnitCode(String unitCode) 
    {
        this.unitCode = unitCode;
    }

    public String getUnitCode() 
    {
        return unitCode;
    }
    public void setUnitLevel(int unitLevel)
    {
        this.unitLevel = unitLevel;
    }

    public int getUnitLevel()
    {
        return unitLevel;
    }
    public void setUnitAcreage(BigDecimal unitAcreage) 
    {
        this.unitAcreage = unitAcreage;
    }

    public BigDecimal getUnitAcreage() 
    {
        return unitAcreage;
    }
    public void setUnitHaveElevator(String unitHaveElevator) 
    {
        this.unitHaveElevator = unitHaveElevator;
    }

    public String getUnitHaveElevator() 
    {
        return unitHaveElevator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("unitId", getUnitId())
            .append("communityId", getCommunityId())
            .append("buildingId", getBuildingId())
            .append("unitName", getUnitName())
            .append("unitCode", getUnitCode())
            .append("unitLevel", getUnitLevel())
            .append("unitAcreage", getUnitAcreage())
            .append("unitHaveElevator", getUnitHaveElevator())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}

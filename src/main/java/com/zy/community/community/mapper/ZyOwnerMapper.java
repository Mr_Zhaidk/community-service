package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyOwner;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author yangdi
 */
public interface ZyOwnerMapper extends BaseMapper<ZyOwner> {

    @Select("select owner_id from zy_owner where owner_open_id=#{openId}")
    Long findOwnerIdByOpenId(@Param("openId") String openId);
}

package com.zy.community.web.controller.community;
import java.util.List;
import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.service.IZyFilesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件管理Controller
 * 
 * @author yin
 * @date 2020-12-17
 */
@Api(tags = "文件")
@RestController
@RequestMapping("/system/files")
public class ZyFilesController extends BaseController
{
    @Autowired
    private IZyFilesService zyFilesService;

    /**
     * 查询文件管理列表
     */
    @ApiOperation(value = "查询文件管理列表")
    @PreAuthorize("@ss.hasPermi('system:files:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyFiles zyFiles)
    {
        startPage();
        List<ZyFiles> list = zyFilesService.selectZyFilesList(zyFiles);
        return getDataTable(list);
    }

    /**
     * 获取文件管理详细信息
     */
    @ApiOperation(value = "获取文件管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:files:query')")
    @GetMapping(value = "/{filesId}")
    public ZyResult<ZyFiles> getInfo(@PathVariable("filesId") Long filesId)
    {
        return ZyResult.data(zyFilesService.selectZyFilesById(filesId));
    }

    /**
     * 新增文件管理
     */
    @ApiOperation(value = "新增文件管理")
    @PreAuthorize("@ss.hasPermi('system:files:add')")
    @Log(title = "文件管理", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult<String> add(@RequestBody ZyFiles[] zyFiles)
    {
       return toZyAjax(zyFilesService.insertZyFiles(zyFiles));
    }

    /**
     * 修改文件管理
     */
    @ApiOperation(value = "修改文件管理")
    @PreAuthorize("@ss.hasPermi('system:files:edit')")
    @Log(title = "文件管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyFiles zyFiles)
    {
        return toZyAjax(zyFilesService.updateZyFiles(zyFiles));
    }

    /**
     * 删除文件管理
     */
    @ApiOperation(value = "删除文件管理")
    @PreAuthorize("@ss.hasPermi('system:files:remove')")
    @Log(title = "文件管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{filesIds}")
    public ZyResult remove(@PathVariable Long[] filesIds)
    {
        return toZyAjax(zyFilesService.deleteZyFilesByIds(filesIds));
    }
}

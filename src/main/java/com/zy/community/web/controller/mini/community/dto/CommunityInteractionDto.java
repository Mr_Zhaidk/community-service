package com.zy.community.web.controller.mini.community.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommunityInteractionDto implements Serializable {


    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long interactionId;

    /** 小区ID */
    @Excel(name = "小区ID")
    @NotNull(message = "小区Id不能为空")
    private Long communityId;

    /** 内容 */
    @Excel(name = "内容")
    @NotBlank(message = "互动内容不能为空")
    private String content;




    private List<String> imageUrls = new ArrayList<>();

    public Long getInteractionId() {
        return interactionId;
    }

    public void setInteractionId(Long interactionId) {
        this.interactionId = interactionId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }





    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }
}

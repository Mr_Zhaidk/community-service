package com.zy.community.web.controller.mini.login.dto;

import java.io.Serializable;

/**
 * 登录的Dto
 */
public class LoginDto implements Serializable {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

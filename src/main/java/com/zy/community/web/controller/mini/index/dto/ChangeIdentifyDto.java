package com.zy.community.web.controller.mini.index.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 变更身份的信息
 */
public class ChangeIdentifyDto implements Serializable {

    @NotNull(message = "绑定Id不能为空")
    private Long ownerRoomId;
    @NotBlank(message = "业主类型不能为空")
    private String ownerType;


    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }
}

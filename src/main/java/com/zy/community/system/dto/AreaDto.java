package com.zy.community.system.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 区域层级模型
 */
public class AreaDto implements Serializable {
    //区划码
    private Integer code;
    //区划名称
    private String name;
    //子区划
    private List<AreaDto> children;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AreaDto> getChildren() {
        return children;
    }

    public void setChildren(List<AreaDto> children) {
        this.children = children;
    }
}

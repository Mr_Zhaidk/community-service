package com.zy.community.mini.service.my.basic;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.utils.StringUtils;
import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.framework.security.mini.MiniContextUtils;
import com.zy.community.mini.factory.OwnerFactory;
import com.zy.community.web.controller.mini.login.dto.MiniUserDto;
import com.zy.community.web.controller.mini.my.basic.dto.UserBasicDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 业主基础信息处理服务
 */
@Service
public class MiniBasicService {
    @Resource
    private ZyOwnerMapper zyOwnerMapper;
    @Resource
    private OwnerFactory ownerFactory;


    @Transactional
    public ZyResult<MiniUserDto> editBasicInfo(UserBasicDto dto) {
        String openId = MiniContextUtils.getOpenId();
        if (StringUtils.isEmpty(openId)) return ZyResult.fail(401,"用户未认证");
        ZyOwner zyOwner = zyOwnerMapper.selectOne(new QueryWrapper<ZyOwner>().eq("owner_open_id", openId));
        if (zyOwner == null) {
            return ZyResult.fail(404, "用户不存在");
        }
        zyOwner.setUpdateTime(new Date());
        ownerFactory.transFromBasicDtoWithZyOwner(dto, zyOwner);
        zyOwnerMapper.updateById(zyOwner);
        return ZyResult.data(ownerFactory.transFromZyOwner(zyOwner));
    }

}

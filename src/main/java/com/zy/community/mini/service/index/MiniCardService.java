package com.zy.community.mini.service.index;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.vo.RoomStatus;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.mapper.ZyOwnerRoomMapper;
import com.zy.community.community.mapper.ZyOwnerRoomRecordMapper;
import com.zy.community.framework.security.mini.MiniContextUtils;
import com.zy.community.mini.factory.BindFactory;
import com.zy.community.web.controller.mini.index.dto.CardDto;
import com.zy.community.web.controller.mini.index.dto.ChangeIdentifyDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 身份卡的服务
 */
@Service
public class MiniCardService {
    @Resource
    private ZyOwnerMapper zyOwnerMapper;
    @Resource
    private ZyOwnerRoomMapper zyOwnerRoomMapper;
    @Resource
    private ZyOwnerRoomRecordMapper zyOwnerRoomRecordMapper;
    @Resource
    private BindFactory bindFactory;

    /**
     * 获取当前业主的身份卡信息
     *
     * @param communityId 小区Id
     * @return 身份卡信息
     */
    public ZyResult<List<CardDto>> findCurrentOwnerCardInfo(Long communityId) {
        if (communityId == null) return ZyResult.fail(400, "社区ID不能为空");
        String openId = MiniContextUtils.getOpenId();
        if (StringUtils.isEmpty(openId)) return ZyResult.fail(401, "用户未认证");
        ZyOwner zyOwner = zyOwnerMapper.selectOne(new QueryWrapper<ZyOwner>().eq("owner_open_id", openId));
        if (zyOwner == null) return ZyResult.fail(404, "该用户不存在");
        List<CardDto> ownerCardInfo = zyOwnerRoomMapper.findOwnerCardInfo(communityId, zyOwner.getOwnerId());
        return ZyResult.data(ownerCardInfo);
    }

    /**
     * 变更身份申请
     * @param changeIdentifyDto 变更dto
     * @return 变更结果
     */
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public ZyResult<String> changeIdentify(ChangeIdentifyDto changeIdentifyDto){
        String openId = MiniContextUtils.getOpenId();
        if (StringUtils.isEmpty(openId)) return ZyResult.fail(401, "用户未认证");
        ZyOwner zyOwner = zyOwnerMapper.selectOne(new QueryWrapper<ZyOwner>().eq("owner_open_id", openId));
        if (zyOwner == null) return ZyResult.fail(404, "该用户不存在");
        ZyOwnerRoom zyOwnerRoom = zyOwnerRoomMapper.selectById(changeIdentifyDto.getOwnerRoomId());
        if (zyOwnerRoom==null) return ZyResult.fail(404,"绑定信息不存在");
        synchronized (this){
            if("yz".equals(changeIdentifyDto.getOwnerType())){
                //如果想切换身份为业主,需要判断当前房屋是否有业主申请
                ZyOwnerRoom zyOwnerRoom1 = zyOwnerRoomMapper.selectOne(new QueryWrapper<ZyOwnerRoom>()
                        .eq("room_id", zyOwnerRoom.getRoomId())
                        .eq("owner_type", "yz")
                        .eq("room_status", RoomStatus.Binding.name())
                );
                if (zyOwnerRoom1!=null) return ZyResult.fail(400,"当前房屋已有业主绑定");
            }
            zyOwnerRoomMapper.update(null,new UpdateWrapper<ZyOwnerRoom>()
            .eq("owner_room_id",changeIdentifyDto.getOwnerRoomId())
                    .set("update_time",new Date())
                    .set("owner_type",changeIdentifyDto.getOwnerType())
                    .set("room_status", RoomStatus.Auditing)
            );
        }
        zyOwnerRoom.setRoomStatus(RoomStatus.Auditing);
        zyOwnerRoom.setOwnerType(changeIdentifyDto.getOwnerType());
        zyOwnerRoom.setUpdateTime(new Date());
        ZyOwnerRoomRecord bindRecord = bindFactory.createBindRecord(zyOwnerRoom);
        bindRecord.setCreateById(zyOwner.getOwnerId());
        zyOwnerRoomRecordMapper.insert(bindRecord);
        return ZyResult.success("身份变更申请已提出");
    }
}
